ARG DOCKER_BASE_IMAGE_PREFIX
ARG DOCKER_BASE_IMAGE_NAMESPACE=pythonpackagesonalpine
ARG DOCKER_BASE_IMAGE_NAME=basic-python-packages-pre-installed-on-alpine
ARG DOCKER_BASE_IMAGE_TAG=pip-alpine
FROM ${DOCKER_BASE_IMAGE_PREFIX}${DOCKER_BASE_IMAGE_NAMESPACE}/${DOCKER_BASE_IMAGE_NAME}:${DOCKER_BASE_IMAGE_TAG}

ARG FIX_ALL_GOTCHAS_SCRIPT_LOCATION
ARG ETC_ENVIRONMENT_LOCATION
ARG CLEANUP_SCRIPT_LOCATION

# Depending on the base image used, we might lack wget/curl/etc to fetch ETC_ENVIRONMENT_LOCATION.
ADD $FIX_ALL_GOTCHAS_SCRIPT_LOCATION .
ADD $CLEANUP_SCRIPT_LOCATION .

RUN set -o allexport \
    && . ./fix_all_gotchas.sh \
    && set +o allexport \
    && apk add --no-cache py3-pyzmq jupyter-nbconvert py3-ipykernel \
    # && python -m pip install --no-cache-dir --break-system-packages nbconvert \
    # We'll always need ipykernel when sharing notebooks so that everyone can still run the notebook locally.
    # && python -m pip install --no-cache-dir --break-system-packages ipykernel \
    # && python -m pip install --no-cache-dir --break-system-packages jupyter-autotime \
    # && python -m pip install --no-cache-dir --break-system-packages jupyter_contrib_nbextensions \
    # && python -m jupyter contrib nbextension install \
    # && python -m jupyter nbextension enable execute_time/ExecuteTime \
    && python -m pip install --no-cache-dir --break-system-packages jupyterlab_execute_time \
    && python -m jupyter labextension enable jupyterlab_execute_time \
    && . ./cleanup.sh

